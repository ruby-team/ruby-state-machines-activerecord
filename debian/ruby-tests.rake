require 'gem2deb/rake/testtask'

require 'fileutils'
FileUtils.mkdir_p("log")

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['lib', 'test']
  t.test_files = FileList['test/*_test.rb'] 
end
